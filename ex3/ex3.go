// 1. Доработать калькулятор: больше операций и валидации данных.

package main

import (
	"fmt"
	"math"
	"os"
)

func main() {
	var a, b, res, resA, resB uint8 //change type from float64 to uint8 for %, ++, -- operations
	var resPow float64
	var op string

	fmt.Print("Введите первое число: ")
	fmt.Scanln(&a)

	fmt.Print("Введите второе число: ")
	fmt.Scanln(&b)

	fmt.Print(`Введите арифметическую операцию (+, -, *, /, 
		Остаток от деления A на B (%), 
		Квадрат числа(**), 
		A в степени B(ab), 
		Инкремент(++) A+1, B+1,
		Декремент(--)) A-1, B-1: `)

	fmt.Scanln(&op)

	switch op {
	case "+":
		res = a + b
	case "-":
		res = a - b
	case "*":
		res = a * b
	case "/":
		res = a / b
	// added operations from ex3 task
	case "%":
		res = a % b
	case "**":
		resA = a * a
		resB = b * b
	case "ab":
		var a float64 = float64(a)
		var b float64 = float64(b)
		// для возведения в степень пришлось ввести доп переменную float64, по другому не знаю как сделать
		resPow = math.Pow(a, b)
	case "++":
		a++
		b++
		resA = a
		resB = b
	case "--":
		a--
		b--
		resA = a
		resB = b
	default:
		fmt.Println("Операция выбрана неверно")
		os.Exit(1)
	}
	if op == "++" {
		fmt.Printf("Результат выполнения операции: a = %d, b = %d\n", resA, resB)
	} else if op == "--" {
		fmt.Printf("Результат выполнения операции: a = %d, b = %d\n", resA, resB)
	} else if op == "**" {
		fmt.Printf("Результат выполнения операции: a = %d, b = %d\n", resA, resB)
	} else if op == "ab" {
		fmt.Printf("Результат выполнения операции: %f\n", resPow)
	} else {
		fmt.Printf("Результат выполнения операции: %d\n", res)
	}
}
